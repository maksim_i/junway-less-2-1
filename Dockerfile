FROM python:3
ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE 1
WORKDIR /django-blog
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
